package de.shinyqb.bannsystem.commands;

import de.shinyqb.bannsystem.utils.BannManager;
import de.shinyqb.bannsystem.utils.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class UnBanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender,Command command, String label, String[] args) {
        if(args.length == 1){
            int player_id = BannManager.getIDByName(args[0]);
            if(player_id == 0){
                sender.sendMessage(Config.get("prefix") + "Spieler konnte nicht entbannt werden");
            }
            else {
                BannManager.unBanPlayer(player_id);
            }
        }
        else{
            sender.sendMessage(Config.get("prefix") + "Der Kommandobefehl wurde falsch verwendet. Bitte nutze /unban <Spieler>");
        }
        return true;
    }
}
