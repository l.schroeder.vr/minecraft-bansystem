package de.shinyqb.bannsystem.commands;

import de.shinyqb.bannsystem.utils.BannManager;
import de.shinyqb.bannsystem.utils.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TempBanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 3){
            BannManager.banPlayer(Bukkit.getPlayer(args[0]), args[1], Integer.parseInt(args[2])); // Bug bei Offline Banns
        }
        else{
            sender.sendMessage(Config.get("prefix") + "Der Kommandobefehl wurde falsch verwendet. Bitte nutze /tempban <Spieler> <Grund> <Dauer in Stunden>");
        }
        return true;
    }
}
