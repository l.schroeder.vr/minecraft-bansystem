package de.shinyqb.bannsystem.commands;

import de.shinyqb.bannsystem.utils.BannManager;
import de.shinyqb.bannsystem.utils.Config;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class BanCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if(args.length == 2){
                BannManager.banPlayer(Bukkit.getPlayer(args[0]), args[1]); // Bug bei Offline Banns
            }
        else{
            sender.sendMessage(Config.get("prefix") + "Der Kommandobefehl wurde falsch verwendet. Bitte nutze /ban <Spieler> <Grund>");
        }
        return true;
    }
}
