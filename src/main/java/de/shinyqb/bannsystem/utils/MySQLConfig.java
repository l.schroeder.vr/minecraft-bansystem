package de.shinyqb.bannsystem.utils;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class MySQLConfig {

    private static File file;
    private static YamlConfiguration db_config;

    public MySQLConfig() {
        File dir = new File("./plugins/BannSystem/");
        if (!dir.exists()) {
            dir.mkdirs();
        }

        file = new File(dir, "db_config.yml");
        if (!file.exists()) {
            try {
                file.createNewFile();
                db_config = YamlConfiguration.loadConfiguration(file);
                setStandardDBConfig();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            db_config = YamlConfiguration.loadConfiguration(file);
        }
    }

    public static void setStandardDBConfig(){
        db_config.options().copyDefaults(true);
        db_config.addDefault("username", "root");
        db_config.addDefault("password", "");
        db_config.addDefault("database", "db_bannsystem");
        db_config.addDefault("host", "localhost");
        db_config.addDefault("port", "3306");
        try {
            db_config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean contains(String path){
        return db_config.contains(path);
    }

    public static void set(String path, Object value){
        db_config.set(path, value);
        try {
            db_config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object get(String path){
        if(!contains(path)){
            return null;
        }
        return db_config.get(path);
    }

}
