package de.shinyqb.bannsystem.utils;

import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class Config {

    private static File file;
    private static YamlConfiguration config;

    public Config() {
        File dir = new File( "./plugins/BannSystem/");
        if(!dir.exists()){
            dir.mkdirs();
        }

        file = new File(dir, "config.yml");
        if(!file.exists()){
            try {
                file.createNewFile();
                config = YamlConfiguration.loadConfiguration(file);
                setStandardConfig();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else{
            config = YamlConfiguration.loadConfiguration(file);
        }
    }

    public static void setStandardConfig(){
        config.options().copyDefaults(true);
        config.addDefault("prefix", ChatColor.DARK_GRAY + "[" + ChatColor.GOLD + "BannSystem" + ChatColor.DARK_GRAY + "] " + ChatColor.GRAY );
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static boolean contains(String path){
        return config.contains(path);
    }

    public static void set(String path, Object value){
        config.set(path, value);
        try {
            config.save(file);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Object get(String path){
        if(!contains(path)){
            return null;
        }
        return config.get(path);
    }
}
