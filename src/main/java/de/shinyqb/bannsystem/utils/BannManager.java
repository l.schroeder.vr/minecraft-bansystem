package de.shinyqb.bannsystem.utils;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

public class BannManager {

    public static int isPlayerInDatabase(Player player){
        String uuid = player.getUniqueId().toString();
        String querry = "SELECT * FROM `tbl_players` WHERE tbl_players.uuid=" + "'" + uuid + "'";
        ResultSet results = MySQL.showResults(querry);
        try {
            if(!results.next()){
                createPlayerEntry(player);
                return isPlayerInDatabase(player);
            }
            else{
                int id_player = 0;
                id_player = Integer.parseInt(results.getString("id_player"));
                return id_player;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static void createPlayerEntry(Player player){
        String uuid = player.getUniqueId().toString();
        String name = player.getName();
        if(player.isOp()){
            String querry = "INSERT INTO tbl_players (uuid,name,is_op) VALUES ('"+uuid+"','"+name+"','1')";
            MySQL.update(querry);
        }
        else{
            String querry = "INSERT INTO tbl_players (uuid,name,is_op) VALUES ('"+uuid+"','"+name+"','0')";
            MySQL.update(querry);
        }

    }

    public static int isPlayerBanned(int id_player) {
        String querry = "SELECT * FROM `tbl_banns` WHERE tbl_banns.player_id=" + "'" + id_player + "'";
        ResultSet results = MySQL.showResults(querry);
        try {
            while(results.next()){
                if(results.getDate("unban_timestamp") == null){
                    return Integer.parseInt(results.getString("id_ban"));
                }
                Date unban_timestemp = results.getDate("unban_timestamp");
                Date now = new Date();
                if(!now.after(unban_timestemp)){
                    return Integer.parseInt(results.getString("id_ban"));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return 0;
    }

    public static String getBanReason(int id_ban){
        String querry = "SELECT * FROM `tbl_banns` WHERE tbl_banns.id_ban=" + "'" + id_ban + "'";
        ResultSet results = MySQL.showResults(querry);
        try {
            while(results.next()){
                return results.getString("reason");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getRemainingBanTime(int id_ban){
        String querry = "SELECT * FROM `tbl_banns` WHERE tbl_banns.id_ban=" + "'" + id_ban + "'";
        ResultSet results = MySQL.showResults(querry);
        try {
            while(results.next()){
                if(results.getString("unban_timestamp") == null){
                    return "Dein Ban permanent!";
                }
                return results.getString("unban_timestamp");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return "";
    }

    public static void banPlayer(Player player, String reason){
        String querry = "INSERT INTO tbl_banns (player_id,reason) VALUES ('"+isPlayerInDatabase(player)+"','"+reason+"')";
        MySQL.update(querry);

        int id_ban = BannManager.isPlayerBanned(isPlayerInDatabase(player));
        if(id_ban != 0){
            player.kickPlayer("Du wurdest von diesem Server gebannt!" +
                    "\n" +
                    "Gebannt bis: " + BannManager.getRemainingBanTime(id_ban) +
                    "\n" +
                    "Grund: " + BannManager.getBanReason(id_ban)
            );
        }
    }

    public static void banPlayer(Player player, String reason, int duration){
        Calendar cal = Calendar.getInstance();
        cal.setTime(new Date());
        cal.add(Calendar.HOUR_OF_DAY, duration);
        Timestamp unban_date = new Timestamp(cal.getTime().getTime());
        String querry = "INSERT INTO tbl_banns (player_id,reason,unban_timestamp) VALUES ('"+isPlayerInDatabase(player)+"','"+reason+"','"+unban_date+"')";
        MySQL.update(querry);

        int id_ban = BannManager.isPlayerBanned(isPlayerInDatabase(player));
        if(id_ban != 0){
            player.kickPlayer("Du wurdest von diesem Server gebannt!" +
                    "\n" +
                    "Gebannt bis: " + BannManager.getRemainingBanTime(id_ban) +
                    "\n" +
                    "Grund: " + BannManager.getBanReason(id_ban)
            );
        }
    }

    public static void unBanPlayer(int player_id){
        Date nowDate = new Date();
        Timestamp now = new Timestamp(nowDate.getTime());

        int id_ban = isPlayerBanned(player_id);

        String querry = "UPDATE tbl_banns SET unban_timestamp = '" + now + "' WHERE id_ban = '" + id_ban + "'";
        MySQL.update(querry);
    }

    public static int getIDByName(String name){
        String querry = "SELECT * FROM `tbl_players` WHERE tbl_players.name=" + "'" + name + "'";
        ResultSet results = MySQL.showResults(querry);
        try {
            if(!results.next()){
                return 0;
            }
            else{
                int id_player = 0;
                id_player = Integer.parseInt(results.getString("id_player"));
                return id_player;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return 0;
    }

}
