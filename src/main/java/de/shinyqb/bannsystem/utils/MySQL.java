package de.shinyqb.bannsystem.utils;

import org.bukkit.Bukkit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class MySQL {

    public static String username;
    public static String password;
    public static String database;
    public static String host;
    public static String port;
    public static Connection con;

    public MySQL(){
        username = (String)MySQLConfig.get("username");
        password = (String)MySQLConfig.get("password");
        database = (String)MySQLConfig.get("database");
        host = (String)MySQLConfig.get("host");
        port = (String)MySQLConfig.get("port");

        connect();
    }

    public static void connect(){
        if(!isConnected()){
            try {
                con = DriverManager.getConnection("jdbc:mysql://" + host + ":" + port + "/" + database, username, password);
                Bukkit.getConsoleSender().sendMessage((String)Config.get("prefix") + "Database connected!");
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public static void close(){
        if(isConnected()){
            try {
                con.close();
                Bukkit.getConsoleSender().sendMessage((String)Config.get("prefix") + "Database connection closed!");
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }
    }

    public static boolean isConnected(){
        return (con != null);
    }

    public static Connection getConnection(){
        return con;
    }

    public static void createDatabase(){
        // Tabelle Players
        try {
            con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS tbl_players ( `id_player` INT NOT NULL AUTO_INCREMENT , `uuid` VARCHAR(100) NOT NULL , `name` VARCHAR(100) NOT NULL , `is_op` BOOLEAN NOT NULL , PRIMARY KEY (`id_player`))");
        } catch (SQLException e) {
            e.printStackTrace();
        }

        // Tabelle Banns mir Reference auf tbl_player
        try {
            con.createStatement().executeUpdate("CREATE TABLE IF NOT EXISTS tbl_banns ( `id_ban` INT NOT NULL AUTO_INCREMENT , `player_id` INT NOT NULL , `reason` VARCHAR(255) NULL , `ban_timestamp` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , `unban_timestamp` DATETIME NULL , PRIMARY KEY (`id_ban`), FOREIGN KEY (player_id) REFERENCES tbl_players(id_player))");
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static ResultSet showResults(String querry){
        if(isConnected()){
            try {
                return con.createStatement().executeQuery(querry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    public static void update(String querry){
        if(isConnected()){
            try {
                con.createStatement().executeUpdate(querry);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }


}
