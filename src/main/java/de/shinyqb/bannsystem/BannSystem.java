package de.shinyqb.bannsystem;

import de.shinyqb.bannsystem.commands.BanCommand;
import de.shinyqb.bannsystem.commands.TempBanCommand;
import de.shinyqb.bannsystem.commands.UnBanCommand;
import de.shinyqb.bannsystem.listeners.JoinListener;
import de.shinyqb.bannsystem.utils.Config;
import de.shinyqb.bannsystem.utils.MySQL;
import de.shinyqb.bannsystem.utils.MySQLConfig;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.sql.SQLException;

public final class BannSystem extends JavaPlugin {

    @Override
    public void onEnable() {
        new Config();
        new MySQLConfig();
        new MySQL();
        MySQL.createDatabase();

        Bukkit.getConsoleSender().sendMessage((String)Config.get("prefix") + ChatColor.GREEN + "Plugin enabled!");

        registerListeners();
        registerCommands();
    }

    @Override
    public void onDisable() {
        MySQL.close();
    }

    public void registerListeners(){
        PluginManager pluginManager = Bukkit.getPluginManager();
        pluginManager.registerEvents(new JoinListener(), this);
    }

    public void registerCommands(){
        getCommand("ban").setExecutor(new BanCommand());
        getCommand("unban").setExecutor(new UnBanCommand());
        getCommand("tempban").setExecutor(new TempBanCommand());
    }

}
