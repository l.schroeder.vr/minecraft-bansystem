package de.shinyqb.bannsystem.listeners;

import de.shinyqb.bannsystem.utils.BannManager;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class JoinListener implements Listener {

    @EventHandler
    public void onJoin(PlayerJoinEvent event){
        Player player = event.getPlayer();

        int id_player = BannManager.isPlayerInDatabase(player);
        int id_ban = BannManager.isPlayerBanned(id_player);
        if(id_ban != 0){
            player.kickPlayer("Du wurdest von diesem Server gebannt!" +
            "\n" +
            "Gebannt bis: " + BannManager.getRemainingBanTime(id_ban) +
            "\n" +
            "Grund: " + BannManager.getBanReason(id_ban)
            );
        }
    }

}